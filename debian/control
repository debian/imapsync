Source: imapsync
Section: mail
Priority: optional
Maintainer: Micke Nordin <kano@sunet.se>
Uploaders: Jakob Haufe <sur5r@debian.org>,
           Maytham Alsudany <maytha8thedev@gmail.com>
Build-Depends: debhelper-compat (= 13),
               libcgi-pm-perl,
               libcrypt-openssl-rsa-perl,
               libdata-uniqid-perl,
               libdigest-hmac-perl,
               libencode-imaputf7-perl,
               libfile-copy-recursive-perl,
               libfile-tail-perl,
               libhtml-parser-perl,
               libio-socket-inet6-perl,
               libio-socket-ssl-perl,
               libio-tee-perl,
               libjson-webtoken-perl,
               libmail-imapclient-perl,
               libmodule-scandeps-perl,
               libparse-recdescent-perl,
               libproc-processtable-perl,
               libreadonly-perl,
               libregexp-common-perl,
               libsys-meminfo-perl,
               libterm-readkey-perl,
               libtest-deep-perl,
               libtest-mockobject-perl,
               libtest-nowarnings-perl,
               libtest-pod-perl,
               libtest-warn-perl,
               libunicode-string-perl,
               liburi-perl,
               libwww-perl,
               time
Standards-Version: 4.7.0
Homepage: https://imapsync.lamiral.info/
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/debian/imapsync.git
Vcs-Browser: https://salsa.debian.org/debian/imapsync

Package: imapsync
Architecture: any
Multi-Arch: foreign
Depends: libauthen-ntlm-perl,
         libcgi-pm-perl,
         libcrypt-openssl-rsa-perl,
         libdata-uniqid-perl,
         libdigest-hmac-perl,
         libencode-imaputf7-perl,
         libfile-copy-recursive-perl,
         libfile-tail-perl,
         libhtml-parser-perl,
         libio-socket-inet6-perl,
         libio-socket-ssl-perl,
         libio-tee-perl,
         libjson-webtoken-perl,
         libmail-imapclient-perl,
         libmodule-scandeps-perl,
         libparse-recdescent-perl,
         libproc-processtable-perl,
         libreadonly-perl,
         libregexp-common-perl,
         libsys-meminfo-perl,
         libterm-readkey-perl,
         libtest-deep-perl,
         libtest-mockobject-perl,
         libtest-nowarnings-perl,
         libtest-pod-perl,
         libtest-warn-perl,
         libunicode-string-perl,
         liburi-perl,
         libwww-perl,
         time,
         ${misc:Depends}
Description: IMAP synchronization, copy and migration tool
 The command imapsync is a tool allowing incremental and recursive imap
 transfer from one mailbox to another.
 .
 We sometimes need to transfer mailboxes from one imap server to another.
 This is called migration.
 .
 imapsync is the adequate tool because it reduces the amount of data
 transferred by not transferring a given message if it is already on both
 sides. Same headers, same message size and the transfer is done only
 once. All flags are preserved, unread will stay unread, read will stay
 read, deleted will stay deleted. You can stop the transfer at any time
 and restart it later, imapsync is adapted to a bad connection.
 .
 You can decide to delete the messages from the source mailbox after a
 successful transfer (it is a good feature when migrating). In that
 case, use the --delete option, and run imapsync again with the --expunge
 option.
 .
 You can also just synchronize a mailbox A from another mailbox B in case
 you just want to keep a "live" copy of B in A (backup).
 .
 Similar packages: offlineimap3, imapcopy.
